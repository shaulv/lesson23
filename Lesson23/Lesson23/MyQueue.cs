using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson23
{
    class MyQueue
    {

        #region Properties
        List<Customer> customers = new List<Customer>();
        #endregion

        #region Methods
        public void Enqueue(Customer customer)
        {
            this.customers.Add(customer);
        }
        public Customer Dequeue()
        {
            Customer lastCurrent = this.customers[0];
            this.customers.RemoveAt(0);
            return lastCurrent;
        }
        public void Init(List<Customer> customers)
        {
            Clear();
            this.customers = customers;
        }
        public void Clear()
        {
            this.customers.Clear();
        }
        public Customer WhoIsnext()
        {
            return this.customers[0];
        }
        public int Count()
        {
            return this.customers.Count;
        }
        public void SortByProtection()
        {
            Init(customers.OrderBy(cur => cur.Protection).ToList());
        }
        public void SortByTotalPurchases()
        {
            Init(customers.OrderBy(cur => cur.TotalPurchases).ToList());
        }
        public void SortByBirthYear()
        {
            Init(customers.OrderByDescending(cur => cur.BirthYear).ToList());
        }
        public List<Customer> DequeueCustomers(int num)
        {
            List<Customer> removed = new List<Customer>();
            if (customers.Count < num)
                return null;

            for (int i = 0; i < num; i++)
            {
                removed.Add(customers[i]);
            }

            customers.RemoveRange(0, num);
            return removed;
        }
        public void AniRakSheela(Customer customer)
        {
            customers.Insert(0, customer);
        }
        public Customer DequeueProtectiza()
        {
            int pos = 0;
            int protect = 0;

            for (int i = 0; i < customers.Count; i++)
            {
                if (customers[i].Protection > protect)
                {
                    protect = customers[i].Protection;
                    pos = i;
                }

            }

            Customer pro = customers[pos];

            customers.RemoveAt(pos);
            return pro;
        }   
        #endregion

    }
}
