﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson23
{
    class Customer
    {
        #region Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public int BirthYear { get; set; }
        public string Address { get; set; }
        public int Protection { get; set; }
        public int TotalPurchases { get; set; }
        #endregion

        #region Constructor
        public Customer(int id, string name, int birthYear, string address, int protection)
        {
            this.Id = id;
            this.Name = name;
            this.BirthYear = birthYear;
            this.Address = address;
            this.Protection = protection;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return $"ID: {this.Id}\nName:\n{this.Name}\nBirth Year: {this.BirthYear}\nAddress: {this.Address}\nProtection: {this.Protection}\nTotal Purchases: {this.TotalPurchases}";
        }
        #endregion

    }
}
